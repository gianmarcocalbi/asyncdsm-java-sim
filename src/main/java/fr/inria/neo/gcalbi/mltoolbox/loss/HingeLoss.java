package fr.inria.neo.gcalbi.mltoolbox.loss;

public class HingeLoss implements LossFunction {

    @Override
    public double[] value() {
        return new double[0];
    }

    @Override
    public double[] gradient() {
        return new double[0];
    }
}
