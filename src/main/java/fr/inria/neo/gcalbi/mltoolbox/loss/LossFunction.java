package fr.inria.neo.gcalbi.mltoolbox.loss;

public interface LossFunction {
    double[] value();
    double[] gradient();
}
