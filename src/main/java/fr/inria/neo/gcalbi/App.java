package fr.inria.neo.gcalbi;



import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.Arrays;
import java.util.Random;

public class App {
    public static void main(String[] args) {
        final Random rand = new Random();
        int n = 10;
        int p = 10;
        int m = 100;

        double[] _w = new double[p];
        Arrays.setAll(_w, (index) -> rand.nextGaussian());

        INDArray w = Nd4j.create(_w);
        INDArray X = Nd4j.zeros(m, p);

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < p; j++) {
                //X[i][j]=rand.nextGaussian();
            }
        }
    }
}
